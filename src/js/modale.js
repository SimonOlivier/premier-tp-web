document.addEventListener("DOMContentLoaded", function(event) {

    var boutonMenuConnexion = document.querySelector('#connexion');
    var boutonConnexion = document.querySelector('#bouton-connexion');


    boutonMenuConnexion.addEventListener('click', function (e) {

        e.preventDefault();
        document.querySelector("#modale-connexion").classList.toggle("u-hidden");

    });

    boutonConnexion.addEventListener('click', function (e) {

        e.preventDefault();
        boutonMenuConnexion.classList.toggle("u-hidden");
        document.querySelector("#modale-connexion").classList.toggle("u-hidden");
        document.querySelector("#profil").classList.toggle("u-hidden");
        document.querySelector("#ajout-article").classList.toggle("u-hidden");
        document.querySelector("#inscription").classList.toggle("u-hidden");

    });



});